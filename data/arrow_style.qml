<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyAlgorithm="0" version="3.14.0-Pi" styleCategories="AllStyleCategories" labelsEnabled="0" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" simplifyDrawingTol="1" readOnly="0" maxScale="0" simplifyLocal="1" minScale="100000000" simplifyDrawingHints="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal enabled="0" durationUnit="min" endExpression="" durationField="" startField="trip_start_datetime" accumulate="0" endField="trip_end_datetime" startExpression="" mode="2" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 symbollevels="0" type="singleSymbol" enableorderby="0" forceraster="0">
    <symbols>
      <symbol force_rhr="0" alpha="1" type="line" clip_to_extent="1" name="0">
        <layer enabled="1" locked="0" pass="0" class="ArrowLine">
          <prop k="arrow_start_width" v="0.6"/>
          <prop k="arrow_start_width_unit" v="MM"/>
          <prop k="arrow_start_width_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="arrow_type" v="0"/>
          <prop k="arrow_width" v="2.2"/>
          <prop k="arrow_width_unit" v="MM"/>
          <prop k="arrow_width_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="head_length" v="0.3"/>
          <prop k="head_length_unit" v="MM"/>
          <prop k="head_length_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="head_thickness" v="1.6"/>
          <prop k="head_thickness_unit" v="MM"/>
          <prop k="head_thickness_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="head_type" v="0"/>
          <prop k="is_curved" v="0"/>
          <prop k="is_repeated" v="1"/>
          <prop k="offset" v="0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="offset_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="ring_filter" v="0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" alpha="1" type="fill" clip_to_extent="1" name="@0@0">
            <layer enabled="1" locked="0" pass="0" class="SimpleFill">
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="0,0,0,255"/>
              <prop k="joinstyle" v="round"/>
              <prop k="offset" v="-1.19999999999999996,1.39999999999999991"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="0,0,0,255"/>
              <prop k="outline_style" v="no"/>
              <prop k="outline_width" v="0.46"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
            <layer enabled="1" locked="0" pass="0" class="SimpleFill">
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,127,0,255"/>
              <prop k="joinstyle" v="round"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="0,0,0,255"/>
              <prop k="outline_style" v="no"/>
              <prop k="outline_width" v="0.46"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory penWidth="0" barWidth="5" width="15" sizeType="MM" labelPlacementMethod="XHeight" showAxis="1" minimumSize="0" penAlpha="255" spacingUnitScale="3x:0,0,0,0,0,0" enabled="0" sizeScale="3x:0,0,0,0,0,0" spacingUnit="MM" rotationOffset="270" backgroundColor="#ffffff" spacing="5" minScaleDenominator="0" direction="0" backgroundAlpha="255" diagramOrientation="Up" scaleBasedVisibility="0" lineSizeType="MM" height="15" lineSizeScale="3x:0,0,0,0,0,0" scaleDependency="Area" opacity="1" penColor="#000000" maxScaleDenominator="1e+08">
      <fontProperties style="" description=".SF NS Text,13,-1,5,50,0,0,0,0,0"/>
      <axisSymbol>
        <symbol force_rhr="0" alpha="1" type="line" clip_to_extent="1" name="">
          <layer enabled="1" locked="0" pass="0" class="SimpleLine">
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" placement="2" showAll="1" zIndex="0" priority="0" obstacle="0" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <referencedLayers/>
  <referencingLayers/>
  <fieldConfiguration>
    <field name="trip_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="trip_date">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="trip_time">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="start_lon">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="end_lon">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="start_lat">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="end_lat">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="length_m">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="start">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="end">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="trip_dayofweek">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="trip_weekdayname">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="hour">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="duration_s">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="trip_start_datetime">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="trip_end_datetime">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="trip_id" index="0"/>
    <alias name="" field="trip_date" index="1"/>
    <alias name="" field="trip_time" index="2"/>
    <alias name="" field="start_lon" index="3"/>
    <alias name="" field="end_lon" index="4"/>
    <alias name="" field="start_lat" index="5"/>
    <alias name="" field="end_lat" index="6"/>
    <alias name="" field="length_m" index="7"/>
    <alias name="" field="start" index="8"/>
    <alias name="" field="end" index="9"/>
    <alias name="" field="trip_dayofweek" index="10"/>
    <alias name="" field="trip_weekdayname" index="11"/>
    <alias name="" field="hour" index="12"/>
    <alias name="" field="duration_s" index="13"/>
    <alias name="" field="trip_start_datetime" index="14"/>
    <alias name="" field="trip_end_datetime" index="15"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="trip_id"/>
    <default applyOnUpdate="0" expression="" field="trip_date"/>
    <default applyOnUpdate="0" expression="" field="trip_time"/>
    <default applyOnUpdate="0" expression="" field="start_lon"/>
    <default applyOnUpdate="0" expression="" field="end_lon"/>
    <default applyOnUpdate="0" expression="" field="start_lat"/>
    <default applyOnUpdate="0" expression="" field="end_lat"/>
    <default applyOnUpdate="0" expression="" field="length_m"/>
    <default applyOnUpdate="0" expression="" field="start"/>
    <default applyOnUpdate="0" expression="" field="end"/>
    <default applyOnUpdate="0" expression="" field="trip_dayofweek"/>
    <default applyOnUpdate="0" expression="" field="trip_weekdayname"/>
    <default applyOnUpdate="0" expression="" field="hour"/>
    <default applyOnUpdate="0" expression="" field="duration_s"/>
    <default applyOnUpdate="0" expression="" field="trip_start_datetime"/>
    <default applyOnUpdate="0" expression="" field="trip_end_datetime"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="trip_id"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="trip_date"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="trip_time"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="start_lon"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="end_lon"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="start_lat"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="end_lat"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="length_m"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="start"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="end"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="trip_dayofweek"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="trip_weekdayname"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="hour"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="duration_s"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="trip_start_datetime"/>
    <constraint exp_strength="0" unique_strength="0" notnull_strength="0" constraints="0" field="trip_end_datetime"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="trip_id"/>
    <constraint exp="" desc="" field="trip_date"/>
    <constraint exp="" desc="" field="trip_time"/>
    <constraint exp="" desc="" field="start_lon"/>
    <constraint exp="" desc="" field="end_lon"/>
    <constraint exp="" desc="" field="start_lat"/>
    <constraint exp="" desc="" field="end_lat"/>
    <constraint exp="" desc="" field="length_m"/>
    <constraint exp="" desc="" field="start"/>
    <constraint exp="" desc="" field="end"/>
    <constraint exp="" desc="" field="trip_dayofweek"/>
    <constraint exp="" desc="" field="trip_weekdayname"/>
    <constraint exp="" desc="" field="hour"/>
    <constraint exp="" desc="" field="duration_s"/>
    <constraint exp="" desc="" field="trip_start_datetime"/>
    <constraint exp="" desc="" field="trip_end_datetime"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" type="field" name="trip_id" hidden="0"/>
      <column width="-1" type="field" name="trip_date" hidden="0"/>
      <column width="-1" type="field" name="trip_time" hidden="0"/>
      <column width="-1" type="field" name="start_lon" hidden="0"/>
      <column width="-1" type="field" name="end_lon" hidden="0"/>
      <column width="-1" type="field" name="start_lat" hidden="0"/>
      <column width="-1" type="field" name="end_lat" hidden="0"/>
      <column width="-1" type="field" name="length_m" hidden="0"/>
      <column width="-1" type="field" name="start" hidden="0"/>
      <column width="-1" type="field" name="end" hidden="0"/>
      <column width="-1" type="field" name="trip_dayofweek" hidden="0"/>
      <column width="-1" type="field" name="trip_weekdayname" hidden="0"/>
      <column width="-1" type="field" name="hour" hidden="0"/>
      <column width="-1" type="field" name="duration_s" hidden="0"/>
      <column width="-1" type="field" name="trip_start_datetime" hidden="0"/>
      <column width="-1" type="field" name="trip_end_datetime" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="duration_s"/>
    <field editable="1" name="end"/>
    <field editable="1" name="end_lat"/>
    <field editable="1" name="end_lon"/>
    <field editable="1" name="hour"/>
    <field editable="1" name="length_m"/>
    <field editable="1" name="start"/>
    <field editable="1" name="start_lat"/>
    <field editable="1" name="start_lon"/>
    <field editable="1" name="trip_date"/>
    <field editable="1" name="trip_dayofweek"/>
    <field editable="1" name="trip_end_datetime"/>
    <field editable="1" name="trip_id"/>
    <field editable="1" name="trip_start_datetime"/>
    <field editable="1" name="trip_time"/>
    <field editable="1" name="trip_weekdayname"/>
  </editable>
  <labelOnTop>
    <field name="duration_s" labelOnTop="0"/>
    <field name="end" labelOnTop="0"/>
    <field name="end_lat" labelOnTop="0"/>
    <field name="end_lon" labelOnTop="0"/>
    <field name="hour" labelOnTop="0"/>
    <field name="length_m" labelOnTop="0"/>
    <field name="start" labelOnTop="0"/>
    <field name="start_lat" labelOnTop="0"/>
    <field name="start_lon" labelOnTop="0"/>
    <field name="trip_date" labelOnTop="0"/>
    <field name="trip_dayofweek" labelOnTop="0"/>
    <field name="trip_end_datetime" labelOnTop="0"/>
    <field name="trip_id" labelOnTop="0"/>
    <field name="trip_start_datetime" labelOnTop="0"/>
    <field name="trip_time" labelOnTop="0"/>
    <field name="trip_weekdayname" labelOnTop="0"/>
  </labelOnTop>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"trip_weekdayname"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
