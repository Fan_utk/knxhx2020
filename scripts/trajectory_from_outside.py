import pandas as pd
import numpy as np
from shapely import wkt
import geopandas as gpd
from geopandas import GeoDataFrame
import matplotlib.pyplot as plt

df = pd.read_csv('./trajclean-coord-origin_and_destinations_guessed.csv')

df = df[df['destination_best_guess'] != df['origin_best_guess']]
df = df[df['destination_best_guess'] == 'Tapp\'d on Cumberland']
df.to_csv('Tappd_only_going.csv')